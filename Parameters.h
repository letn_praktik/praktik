#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <unordered_map>
#include <string>
std::unordered_map<std::string, std::string> loadParameters(std::string parametersFileName);
#endif