#include <stdio.h>
#include <string>
#include <iostream>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
 
using namespace cv;

RNG rng(12345);

int threshold1 = 150, threshold2 = 220;
int dilation_size = 0;
Mat init_img, img, imgBlur, imgGray, imgCanny, imgDil, imgHough, imgThresh, imgMorph1, imgMorph2;
std::vector<Mat> images;
void showImages();

double calculateCircularity(std::vector<cv::Point>& contour) {
    // Calculate the moments of the contour
    Moments moms = moments(contour);

    // Calculate circularity
    double area = moms.m00;
    double perimeter = arcLength(contour, true);
    double circularity = (4 * CV_PI * area) / (perimeter * perimeter);

    return circularity;
}

void updateCanny(){
    Canny(imgMorph2, imgCanny, threshold1, threshold2);
    Mat element = getStructuringElement(
                    MORPH_RECT,
                    Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                    Point( dilation_size, dilation_size )
                    );
    dilate( imgCanny, imgDil, element);
    std::vector<std::vector<Point> > contours;
    std::vector<Vec4i> hierarchy;
    findContours( imgDil, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE );
    Mat drawing = Mat::zeros( imgDil.size(), CV_8UC3 );
    std::cout << "====Contour==== " << contours.size() << ' ' << hierarchy.size() << '\n';
    std::vector<int> cnt_vec(21, 0);
    for( size_t i = 0; i< contours.size(); i++ ){
        if(contourArea(contours[i]) < 30) continue;
        int id = contourArea(contours[i]) / 10;
        if(id >= cnt_vec.size()) id = cnt_vec.size() - 1;
        cnt_vec[id]++;
        //std::vector<Point> approx;
        //approxPolyDP(contours[i], approx, 0.03 * arcLength(contours[i], true), true);
        //if(approx.size() >= 6){
        double circularity = calculateCircularity(contours[i]);
        if(circularity > 0.65){
            drawContours( drawing, contours, (int)i, Scalar(255, 0, 0), 2, LINE_8, hierarchy, 0 );
        }
    }
    std::vector<Vec4i> linesP; // will hold the results of the detection
    HoughLinesP(imgDil, linesP, 1, CV_PI/180, 30, 60, 20 ); // runs the actual detection
    for( size_t i = 0; i < linesP.size(); i++ ){
        Vec4i l = linesP[i];
        line( drawing, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 3, LINE_AA);
    }
    
    images = {imgThresh, imgMorph1, imgMorph2, imgCanny, imgDil, drawing};
    showImages();
}

void onThreshold1Change(int newThreshold1, void*){
    threshold1 = std::min(newThreshold1, threshold2 - 1);
    updateCanny();
    //imshow("Parameters", imgDil);
}

void onThreshold2Change(int newThreshold2, void*){
    threshold2 = std::max(newThreshold2, threshold1 + 1);
    updateCanny();
    //imshow("Parameters", imgDil);
}

void onDilationChange(int newDilation, void*){
    dilation_size = newDilation;
    updateCanny();
    //imshow("Parameters", imgDil);
}


void showImages(){
    int n = images.size();
    int width = 400;
    int height = 300;
    for(Mat& image : images){
        resize(image, image, Size(width, height));
    }
    int rows = 1;
    for (int i = 2; i * i <= n; i++) {
        if (i * i == n){
            rows = i;
            break;
        }
        if (i * i < n && n % i == 0){
            rows = i;
        }
    }
    int cols = images.size() / rows;
    std::cout << "Rows " << rows << " Cols " << cols << '\n';
    Mat canvas(Size(cols * width, rows * height), CV_8UC3, Scalar(255, 255, 255));
    int row = 0, col = 0;
    for(const Mat& image : images){
        Mat image_3ch;
        if (image.channels() == 1){
            cvtColor(image, image_3ch, COLOR_GRAY2BGR);
        }else{
            image_3ch = image;
        }
        Rect roi(col * width, row * height, width, height);
        image_3ch.copyTo(canvas(roi));
        ++col;
        if(col == cols){
            col = 0;
            ++row;
        }
    }
    imshow("Parameters", canvas);
}

int main(int argc, char** argv){
    //setTrackbarPos("Dilation", "Parameters", 2);

    init_img = imread( "../img/img" + std::string(argv[1]) + ".jpg", IMREAD_COLOR );
    std::cout << init_img.empty() << '\n';
    resize(init_img, img, Size(640, 480), INTER_LINEAR);
    GaussianBlur(img, imgBlur, Size(7, 7), 0);
    cvtColor(imgBlur, imgGray, COLOR_BGR2GRAY);
    adaptiveThreshold(imgGray, imgThresh, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 5, 3);
    morphologyEx(imgThresh, imgMorph1, MORPH_OPEN, getStructuringElement(MORPH_RECT, Size(10, 10)));
    morphologyEx(imgMorph1, imgMorph2, MORPH_CLOSE, getStructuringElement(MORPH_RECT, Size(10, 10)));


    namedWindow("Parameters");
    resizeWindow("Parameters", Size(1300, 800));
    createTrackbar("Threshold1", "Parameters", NULL, 255, onThreshold1Change);
    setTrackbarPos("Threshold1", "Parameters", 168);
    createTrackbar("Threshold2", "Parameters", NULL, 255, onThreshold2Change);
    setTrackbarPos("Threshold2", "Parameters", 254);
    createTrackbar("Dilation", "Parameters", NULL, 21, onDilationChange);
    //createTrackbar("");
    updateCanny();
    imshow("Parameters", imgDil);
    waitKey(0);
    destroyAllWindows();
    //showImages(images);
}